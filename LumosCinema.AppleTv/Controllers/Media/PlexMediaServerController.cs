﻿using FrostAura.Standard.Clients.LumosCinema.AppleTV.Controllers.Root;
using LumosCinema.AppleTv.Shared.Services;

namespace FrostAura.Standard.Clients.LumosCinema.AppleTV.Controllers.Media
{
    /// <summary>
    /// Controller for managing system settings related functions
    /// </summary>
    public sealed class PlexMediaServerController: BaseController
    {
        public PlexMediaServerController(string identifier)
            : base(identifier)
        { }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            ConfigurationService
                .Instance
                .PlexConfiguration
                .IsValid
                .Subscribe((newValue) =>
                {
                    // If the configuration is not valid, keep plex content disabled.
                    if (!newValue) return;
                    
                    BeginInvokeOnMainThread(() =>
                    {
                        
                    });
                });
        }
    }
}