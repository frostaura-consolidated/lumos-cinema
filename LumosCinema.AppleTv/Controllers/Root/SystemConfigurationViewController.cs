﻿using System.Collections.Generic;
using FrostAura.Libraries.Core.Extensions.Reactive;
using FrostAura.Libraries.Core.Extensions.Validation;
using LumosCinema.AppleTv.Shared.Extensions.UI.Display;
using LumosCinema.AppleTv.Shared.Extensions.UI.Input;
using LumosCinema.AppleTv.Shared.Extensions.UI.Layout;
using LumosCinema.AppleTv.Shared.Extensions.UI.Status;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using LumosCinema.AppleTv.Shared.Models.Configuration;
using LumosCinema.AppleTv.Shared.Services;
using UIKit;

namespace FrostAura.Standard.Clients.LumosCinema.AppleTV.Controllers.Root
{
    /// <summary>
    /// Controller for managing system settings related functions
    /// </summary>
    public sealed class SystemConfigurationViewController : BaseController
    {
        public SystemConfigurationViewController(string identifier)
            : base(identifier)
        { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            View
                .Clear()
                .Margin(marginContainer =>
                {
                    marginContainer
                        .VerticallySplit(left =>
                        {
                            left
                                .Center((center) =>
                                {
                                    center
                                        .Svg("Settings", 500, 500);
                                });
                        }, right =>
                        {
                            right
                                .Scrollable((scrollContainer) =>
                                {
                                    PlexConfigurationModel plexConfiguration = ConfigurationService
                                        .Instance
                                        .PlexConfiguration;
                                    
                                    plexConfiguration
                                        .ToUiView(scrollContainer);
                                    
                                    scrollContainer
                                        .Section("Phillips Hue".AsObservedValue(), section =>
                                        {
                                            section
                                                .Spinner(true.AsObservedValue())
                                                .Button("Enable Dark Theme".AsObservedValue(), () =>
                                                {
                                                    ThemingService
                                                        .Instance
                                                        .BackgroundColor
                                                        .Value = new HexColorModel { HexString = "#262626" };
                                                    ThemingService
                                                        .Instance
                                                        .PrimaryColor
                                                        .Value = new HexColorModel { HexString = "#F86301" };
                                                });
                                        })
                                        .Section(plexConfiguration.Identifier, section =>
                                        {
                                            section
                                                .TextBox(plexConfiguration.Email, placeholder: "Plex.tv Email".AsObservedValue())
                                                .HorisontalSpace()
                                                .TextBox(plexConfiguration.Password, placeholder: "Plex.tv Password".AsObservedValue(), secure: true)
                                                .HorisontalSpace()
                                                .Button("Test Connection".AsObservedValue(), () =>
                                                {
                                                    var validationResults = new List<ValidationResult>();
                                                    if(ConfigurationService.Instance.PlexConfiguration.)
                                                    {}

                                                    View.Alert("Title", "Some message...", UIAlertActionStyle.Default, null);
                                                });
                                        });
                                });
                        });
                }, padding: 100);
        }
    }
}