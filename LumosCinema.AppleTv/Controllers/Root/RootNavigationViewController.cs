﻿using FrostAura.Standard.Clients.LumosCinema.AppleTV.Controllers.Media;
using UIKit;

namespace FrostAura.Standard.Clients.LumosCinema.AppleTV.Controllers.Root
{
    /// <summary>
    /// Root navigation controller
    /// </summary>
    public class RootNavigationViewController : UITabBarController
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            // Add child view controllers
            AddChildViewController(new PlexMediaServerController("Plex Media Server"));
            AddChildViewController(new SystemConfigurationViewController("Settings"));
        }
    }
}