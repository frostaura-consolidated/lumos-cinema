﻿using FrostAura.Libraries.Core.Extensions.Reactive;
using LumosCinema.AppleTv.Shared.Extensions.UI.Display;
using LumosCinema.AppleTv.Shared.Extensions.UI.Layout;
using LumosCinema.AppleTv.Shared.Services;
using UIKit;

namespace FrostAura.Standard.Clients.LumosCinema.AppleTV.Controllers.Root
{
    /// <summary>
    /// Base controller for all integrations.
    /// </summary>
    public class BaseController : UIViewController
    {
        protected BaseController(string identifier)
        {
            Title = identifier;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            
            ThemingService
                .Instance
                .BackgroundColor
                .Subscribe((newValue) =>
                {
                    BeginInvokeOnMainThread(() => View.BackgroundColor = newValue.ToUiColor());
                });
            
            View
                .Center((centerContainer) =>
                {
                   centerContainer
                       .DisabledLabel("Not Configured".AsObservedValue());
                });
        }
    }
}