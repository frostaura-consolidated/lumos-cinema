﻿using System;
using System.Linq;
using CoreGraphics;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using LumosCinema.AppleTv.Shared.Services;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Input
{
    /// <summary>
    /// Extensions for creating button containers.
    /// </summary>
    public static class ButtonExtensions
    {
        /// <summary>
        /// Create fluent button container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="label">Observable button text.</param>
        /// <param name="onClickHandler">Handler for when the button was clicked.</param>
        /// <param name="containerProcessingAction">Handler for processing container if any.</param>
        /// <returns>View container was created on.</returns>
        public static UIView Button(this UIView parentView,
            IObservedValue<string> label,
            Action onClickHandler,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(0,
                0,
                parentView.Bounds.Width, 
                50f);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If there is a child in the parent container before this textbox.
            if (lastChild != null)
            {
                // Add the padding relative to that last child.
                frame = new CGRect(
                    lastChild.Frame.Left,
                    lastChild.Frame.Bottom,
                    lastChild.Bounds.Width, 
                    50f);
            }

            var container = new UIButton
            {
                Frame = frame,
            };

            container.AllEvents += (sender, args) =>
            {
                onClickHandler?.Invoke();
            };
            
            label.Subscribe((newValue) =>
            {
                container.BeginInvokeOnMainThread(() => container.SetTitle(newValue, UIControlState.Normal));
            });
            
            ThemingService
                .Instance
                .SecondaryColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                    {
                        container.SetTitleColor(newValue.ToUiColor(), UIControlState.Focused);
                    });
                });
            ThemingService
                .Instance
                .PrimaryColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                        {
                            container.SetTitleColor(newValue.ToUiColor(), UIControlState.Normal);
                        });
                });
            ThemingService
                .Instance
                .UnimportantColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                    {
                        container.SetTitleColor(newValue.ToUiColor(), UIControlState.Disabled);
                    });
                });
            ThemingService
                .Instance
                .BackgroundColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                    {
                        container.BackgroundColor = newValue.ToUiColor();
                    });
                });
            
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());
            
            return parentView;
        }
    }
}