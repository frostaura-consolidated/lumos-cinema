﻿using System;
using System.Linq;
using CoreGraphics;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using LumosCinema.AppleTv.Shared.Services;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Input
{
    /// <summary>
    /// Extensions for creating textbox container views.
    /// </summary>
    public static class TextboxExtensions
    {
        /// <summary>
        /// Create fluent textbox container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="value">Observable string that should get updated upon text changed.</param>
        /// <param name="placeholder">Observable value for textbox placeholder. Upon update, the placeholder text will update.</param>
        /// <param name="secure">Whether or not the text on the textbox should be masked.</param>
        /// <param name="paddingY">Left and right padding of the textbox if any. Use HorisontalSpace extension for top and bottom padding.</param>
        /// <param name="containerProcessingAction">Handler for processing container if any.</param>
        /// <returns>View container was created on.</returns>
        public static UIView TextBox(this UIView parentView,
            IObservedValue<string> value,
            IObservedValue<string> placeholder = null,
            bool secure = false,
            int paddingY = 50,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(paddingY, 0, 0, 0);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If there is a child in the parent container before this textbox.
            if (lastChild != null)
            {
                // Add the padding relative to that last child.
                frame = new CGRect(
                    paddingY,
                    lastChild.Frame.Bottom,
                    0, 0);
            }

            var container = new UITextField
            {
                Placeholder = placeholder?.Value,
                BorderStyle = UITextBorderStyle.RoundedRect,
                Frame = frame,
                SecureTextEntry = secure,
                BackgroundColor = UIColor.Blue
            };

            //Color config
            ThemingService
                .Instance
                .SecondaryColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                    {
                        container.TextColor = newValue.ToUiColor();
                        container.BackgroundColor = newValue.ToUiColor(50);
                    });
                });
            
            // Wire up listener for when placeholder observable updates.
            placeholder?.Subscribe(onChangedHandler: (newvalue) =>
            {
                container.BeginInvokeOnMainThread(() => container.Placeholder = newvalue);
            });
            
            // Wire up event handler to observable link.
            container.EditingDidEnd += (sender, args) =>
            {
                value.Value = container.Text;
            };
            
            container.SizeToFit();
            container.Frame = new CGRect(
                container.Frame.Left,
                container.Frame.Top,
                parentView.Bounds.Width - (paddingY * 2),
                container.Frame.Height);
            
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());
            
            // Return parent for fluent programming.
            return parentView;
        }
    }
}