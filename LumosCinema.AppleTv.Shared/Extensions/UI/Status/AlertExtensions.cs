﻿using System;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Status
{
    /// <summary>
    /// Extensions for showing alert containers.
    /// </summary>
    public static class AlertExtensions
    {
        /// <summary>
        /// Create fluent alert container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="title">Alert main title.</param>
        /// <param name="message">Alert main message text.</param>
        /// <param name="alertStyle">Alert style.</param>
        /// <param name="okAction">Hanler for when ok has been clicked.</param>
        /// <param name="cancelAction">Hanler for when cancelledß has been clicked.</param>
        /// <param name="customOkTitle">Override OK text.</param>
        /// <returns>View container was created on.</returns>
        public static UIView Alert(this UIView parentView,
            string title,
            string message,
            UIAlertActionStyle alertStyle,
            Action okAction,
            Action cancelAction = null,
            string customOkTitle = "OK")
        {
            UIAlertController alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            
            alertController.AddAction(UIAlertAction.Create(customOkTitle, UIAlertActionStyle.Default, (obj) =>
            {
                okAction?.Invoke();
            }));

            if (alertStyle == UIAlertActionStyle.Cancel)
            {
                alertController.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, (obj) =>
                {
                    cancelAction?.Invoke();
                }));
            }

            var window= UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;
            while (vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }
            
            vc.PresentViewController(alertController, true, null);
            
            return parentView;
        }
    }
}