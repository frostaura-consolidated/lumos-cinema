﻿using System;
using System.Linq;
using CoreGraphics;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using LumosCinema.AppleTv.Shared.Services;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Status
{
    /// <summary>
    /// Extensions for creating loader container views.
    /// </summary>
    public static class LoaderExtensions
    {
        /// <summary>
        /// Create fluent spinner container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="spinning">Observable value that will control whether or not the loader is loading.</param>
        /// <param name="containerProcessingAction">Handler for processing container if any.</param>
        /// <returns>View container was created on.</returns>
        public static UIView Spinner(this UIView parentView,
            IObservedValue<bool> spinning,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(0, 0, 0, 0);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If another view exists prior to this one, set this view relative to that one.
            if (lastChild != null)
            {
                frame = new CGRect(
                    0,
                    lastChild.Frame.Bottom,
                    0, 0);
            }
            
            var container = new UIActivityIndicatorView
            {
                Frame = frame
            };
            
            ThemingService
                .Instance
                .SecondaryColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                    {
                        container.Color = newValue.ToUiColor();
                    });
                });
            
            // Subscribe to observable spinner value to control animation.
            spinning.Subscribe((newValue) => container.BeginInvokeOnMainThread(() =>
            {
                if(newValue) container.StartAnimating();
                else container.StopAnimating();
            }));
            
            container.StartAnimating();
            container.SizeToFit();
            container.Frame = new CGRect(
                container.Frame.Left,
                container.Frame.Top,
                // Make the spinner full width.
                parentView.Bounds.Width,
                container.Frame.Height);
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());
            
            return parentView;
        }
    }
}