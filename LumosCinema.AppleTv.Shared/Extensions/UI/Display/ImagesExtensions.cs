﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using AVFoundation;
using CoreGraphics;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using LumosCinema.AppleTv.Shared.Services;
using SkiaSharp;
using SkiaSharp.Views.tvOS;
using UIKit;
using SKSvg = SkiaSharp.Extended.Svg.SKSvg;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Display
{
    /// <summary>
    /// Extensions for creating image views.
    /// </summary>
    public static class ImagesExtensions
    {
        /// <summary>
        /// Static SVG file list.
        /// </summary>
        private static List<string> _svgFiles = new List<string>();
        
        /// <summary>
        /// Add a fluent SVG image to a parent view.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="name">Unique identifier of the SVG image.</param>
        /// <param name="width">Width of the image.</param>
        /// <param name="height">Height of the image.</param>
        /// <param name="containerProcessingAction">Handler for processing container if any.</param>
        /// <returns>View to create container on.</returns>
        public static UIView Svg(this UIView parentView,
            string name,
            int width,
            int height,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(0, 0, width, height);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If there is a child in the parent container before this textbox.
            if (lastChild != null)
            {
                // Add the padding relative to that last child.
                frame = new CGRect(
                    0,
                    lastChild.Frame.Bottom,
                    width,
                    height);
            }

            EnsureSvgFiles();

            var container = new SKCanvasView
            {
                Frame = frame
            };
            var svg = new SKSvg();

            // load the SVG document
            string fileName = _svgFiles
                .FirstOrDefault(f => f.Contains(name));

            using (var fileStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(fileName))
            {
                var xmlReaderSettings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Parse,
                    XmlResolver = null
                };

                using (var reader = XmlReader.Create(fileStream, xmlReaderSettings))
                {
                    svg.Load(reader);
                }
            }

            container.PaintSurface += (sender, args) =>
            {
                // Calculate the scaling need to fit
                // Get the size of the canvas
                float canvasMin = Math.Min(args.Info.Width, args.Info.Height);

                // Get the size of the picture
                float svgMax = Math.Max(svg.Picture.CullRect.Width, svg.Picture.CullRect.Height);

                // Get the scale to fill the screen
                float scale = canvasMin / svgMax;

                // Create a scale matrix
                var scaleMatrix = SKMatrix.MakeScale(scale, scale);

                SKColor backgroundColor = ThemingService
                    .Instance
                    .BackgroundColor
                    .Value
                    .ToSkColor();
                SKColor svgColor = ThemingService
                    .Instance
                    .PrimaryColor
                    .Value
                    .ToSkColor();

                var canvas = args.Surface.Canvas;
                
                canvas.Clear(backgroundColor);
                
                // Draw SVG
                using (var paint = new SKPaint()) {
                    paint.ColorFilter = SKColorFilter.CreateBlendMode(
                        svgColor,       // the color, also `(SKColor)0xFFFF0000` is valid
                        SKBlendMode.SrcIn); // use the source color

                    args
                        .Surface
                        .Canvas
                        .DrawPicture(svg.Picture, ref scaleMatrix, paint);
                }
            };
            
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());

            return parentView;
        }

        /// <summary>
        /// Grab all SVG files from the assembly.
        /// </summary>
        private static void EnsureSvgFiles()
        {
            if (_svgFiles.Any()) return;
            
            _svgFiles = Assembly
                .GetExecutingAssembly()
                .GetManifestResourceNames()
                .Where(n => n.EndsWith(".svg"))
                .ToList();
        }
    }
}