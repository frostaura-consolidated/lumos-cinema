﻿using System;
using System.Linq;
using CoreGraphics;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using LumosCinema.AppleTv.Shared.Services;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Display
{
    /// <summary>
    /// Extensions for creating textbox container views.
    /// </summary>
    public static class LabelExtensions
    {
        /// <summary>
        /// Create fluent title label container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="value">Observable string the label will change to upon update.</param>
        /// <param name="paddingBottom">Bottom padding of the textbox if any.</param>
        /// <param name="containerProcessingAction">Handler for processing container if any.</param>
        /// <returns>View container was created on.</returns>
        public static UIView Title(this UIView parentView,
            IObservedValue<string> value,
            int paddingBottom = 0,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(0, 0, 0, 0);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If there is a child in the parent container before this textbox.
            if (lastChild != null)
            {
                // Add the padding relative to that last child.
                frame = new CGRect(
                    0,
                    lastChild.Frame.Bottom + paddingBottom,
                    0, 0);
            }
            
            var container = new UILabel
            {
                Text = value.Value,
                Font = UIFont.PreferredTitle2,
                Frame = frame
            };
            
            ThemingService
                .Instance
                .PrimaryColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                    {
                        container.TextColor = newValue.ToUiColor();
                    });
                });

            // Wire up listener for when value observable updates.
            value?.Subscribe(onChangedHandler: (newvalue) =>
            {
                container.BeginInvokeOnMainThread(() =>
                {
                    container.Text = newvalue;
                    container.SizeToFit();
                });
            });

            container.SizeToFit();
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());
            
            return parentView;
        }
        
        /// <summary>
        /// Create fluent disabled label container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="value">Observable string the label will change to upon update.</param>
        /// <param name="paddingBottom">Bottom padding of the textbox if any.</param>
        /// <param name="containerProcessingAction">Handler for processing container if any.</param>
        /// <returns>View container was created on.</returns>
        public static UIView DisabledLabel(this UIView parentView,
            IObservedValue<string> value,
            int paddingBottom = 0,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(0, 0, 0, 0);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If there is a child in the parent container before this textbox.
            if (lastChild != null)
            {
                // Add the padding relative to that last child.
                frame = new CGRect(
                    0,
                    lastChild.Frame.Bottom + paddingBottom,
                    0, 0);
            }
            
            var container = new UILabel
            {
                Text = value.Value,
                Font = UIFont.PreferredTitle3,
                Frame = frame
            };
            
            ThemingService
                .Instance
                .UnimportantColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() =>
                    {
                        container.TextColor = newValue.ToUiColor();
                    });
                });

            // Wire up listener for when value observable updates.
            value?.Subscribe(onChangedHandler: (newvalue) =>
            {
                container.BeginInvokeOnMainThread(() =>
                {
                    container.Text = newvalue;
                    container.SizeToFit();
                });
            });

            container.SizeToFit();
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());
            
            return parentView;
        }
    }
}