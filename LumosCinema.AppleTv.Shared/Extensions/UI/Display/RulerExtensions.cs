﻿using System;
using System.Linq;
using CoreGraphics;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using LumosCinema.AppleTv.Shared.Services;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Display
{
    /// <summary>
    /// Extensions for creating ruler container views.
    /// </summary>
    public static class RulerExtensions
    {
        /// <summary>
        /// Create fluent horisontal ruler container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="height">Height of ruler.</param>
        /// <param name="containerProcessingAction">Handler for processing container, if any.</param>
        /// <returns>View container was created on.</returns>
        public static UIView HorisontalRuler(this UIView parentView,
            int height = 2,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(0, 0, parentView.Bounds.Width, height);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If there is a child in the parent container before this textbox.
            if (lastChild != null)
            {
                // Add the padding relative to that last child.
                frame = new CGRect(
                    0,
                    lastChild.Frame.Bottom,
                    parentView.Bounds.Width,
                    height);
            }
            
            UIView container = new UIView
            {
                Frame = frame
            };
            
            ThemingService
                .Instance
                .UnimportantColor
                .Subscribe((newValue) =>
                {
                    container.BeginInvokeOnMainThread(() => container.BackgroundColor = newValue.ToUiColor());
                });
            
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());
            
            return parentView;
        }
    }
}