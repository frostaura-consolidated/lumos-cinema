﻿using System;
using System.Linq;
using CoreGraphics;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Layout
{
    /// <summary>
    /// Extensions to align views.
    /// </summary>
    public static class AlignmentExtensions
    {
        /// <summary>
        /// Create a center container on X and Y axis.
        /// NOTE: All child components of align should be in the conatainer processing action and not on the same level as the align.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="containerProcessingAction">Handler for processing container.></param>
        /// <returns>View margin container was created on.</returns>
        public static UIView Center(this UIView parentView,
            Action<UIView> containerProcessingAction)
        {
            // Create the container.
            var container = new UIView();
            
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            
            // Calculate bounds.
            var height = container
                .Subviews
                .Sum(sv => sv.Bounds.Height);
            var width = container
                .Subviews
                .Select(sv => sv.Bounds.Width)
                .Max();
            
            container.Frame = new CGRect((parentView.Bounds.Width / 2) - (width / 2),
                (parentView.Bounds.Height / 2) - (height / 2),
                width,
                height);
            
            // Add the created view to the parent.
            parentView.Add(container.DecorateForDebugging());
            
            return parentView;
        }
    }
}