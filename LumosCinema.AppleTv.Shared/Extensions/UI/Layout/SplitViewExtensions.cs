﻿using System;
using CoreGraphics;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Layout
{
    /// <summary>
    /// Extensions for creating splitview container views.
    /// </summary>
    public static class SplitViewExtensions
    {
        /// <summary>
        /// Create fluent vertical splitview container.
        /// NOTE: All child components of splitview should be in the conatainer processing action and not on the same level as the splitview.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="leftContainerProcessingAction">Handler for left processing container.</param>
        /// <param name="rightContainerProcessingAction">Handler for right processing container.</param>
        /// <returns>View container was created on.</returns>
        public static UIView VerticallySplit(this UIView parentView,
            Action<UIView> leftContainerProcessingAction,
            Action<UIView> rightContainerProcessingAction)
        {
            // Create the container.
            var left = new UIView(new CGRect(0, 0, parentView.Bounds.Width / 2, parentView.Bounds.Height));
            var right = new UIView(new CGRect(parentView.Bounds.Width / 2, 0, parentView.Bounds.Width / 2, parentView.Bounds.Height));
            
            // Execute the processing that has to occur on the container.
            leftContainerProcessingAction?.Invoke(left);
            rightContainerProcessingAction?.Invoke(right);
            // Add the created view to the parent.
            parentView.Add(left.DecorateForDebugging());
            parentView.Add(right.DecorateForDebugging());
            
            return parentView;
        }
    }
}