﻿using System;
using System.Linq;
using CoreGraphics;
using FrostAura.Libraries.Core.Extensions.Reactive;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using LumosCinema.AppleTv.Shared.Extensions.UI.Display;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Layout
{
    /// <summary>
    /// Extensions for creating section container views.
    /// </summary>
    public static class SectionExtensions
    {
        /// <summary>
        /// Create fluent section container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="title">Container observable container.</param>
        /// <param name="containerProcessingAction">Handler for processing container.</param>
        /// <returns>View container was created on.</returns>
        public static UIView Section(this UIView parentView,
            IObservedValue<string> title,
            Action<UIView> containerProcessingAction)
        {
            parentView
                .Title(title)
                .HorisontalSpace();
            
            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(parentView);
            
            return parentView
                .HorisontalSpace();
        }
    }
}