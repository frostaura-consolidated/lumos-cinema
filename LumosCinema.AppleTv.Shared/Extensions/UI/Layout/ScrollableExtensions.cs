﻿using System;
using CoreGraphics;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using UIKit;
using System.Linq;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Layout
{
    /// <summary>
    /// Extensions for creating scrollable containers.
    /// </summary>
    public static class ScrollableExtensions
    {
        /// <summary>
        /// Create fluent scrollable container.
        /// NOTE: All child components of scroll should be in the conatainer processing action and not on the same level as the scroll.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="containerProcessingAction">Handler for processing container.</param>
        /// <returns>View margin container was created on.</returns>
        public static UIView Scrollable(this UIView parentView,
            Action<UIView> containerProcessingAction)
        {
            // Create the container.
            var container = new UIScrollView
            {
                Frame = new CGRect(0,0, parentView.Bounds.Width, parentView.Bounds.Height),
                ScrollEnabled = true,
            };

            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add a margin to the bottom of the container to display input shadows properly if any.
            container.HorisontalSpace(70);
            
            // Update scroll view content size
            container.ContentSize = new CGSize(parentView.Bounds.Width, container.Subviews.Sum(sv => sv.Bounds.Height));
            
            // Add the created container to the parent.
            parentView.AddSubview(container.DecorateForDebugging());
            
            // Return parent for fluent programming.
            return parentView;
        }
    }
}