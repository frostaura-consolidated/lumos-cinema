﻿using System;
using CoreGraphics;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Layout
{
    /// <summary>
    /// Extensions for creating margin container views.
    /// </summary>
    public static class MarginExtensions
    {
        /// <summary>
        /// Create fluent margin container.
        /// NOTE: A margin container adds a margin to the entirety of the parent container and should be the only child component on it's level.
        /// NOTE: All child components of margin should be in the conatainer processing action and not on the same level as the margin.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="containerProcessingAction">Handler for processing container.</param>
        /// <param name="padding">Padding of container.</param>
        /// <returns>View margin container was created on.</returns>
        public static UIView Margin(this UIView parentView,
            Action<UIView> containerProcessingAction,
            int padding = 0)
        {
            // Create the container.
            var container = new UIView
            {
                Frame = new CGRect(
                    padding,
                    padding,
                    parentView.Bounds.Width - (padding * 2),
                    parentView.Bounds.Height - (padding * 2))
            };

            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created container to the parent.
            parentView.AddSubview(container.DecorateForDebugging());
            
            // Return parent for fluent programming.
            return parentView;
        }
    }
}