﻿using System;
using CoreGraphics;
using LumosCinema.AppleTv.Shared.Extensions.UI.System;
using UIKit;
using System.Linq;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.Layout
{
    /// <summary>
    /// Extensions for creating spacer container views.
    /// </summary>
    public static class SpacerExtensions
    {
        /// <summary>
        /// Create fluent horisontal full-width spacer container.
        /// </summary>
        /// <param name="parentView">View to create container on.</param>
        /// <param name="containerProcessingAction">Handler for processing container.</param>
        /// <param name="height">Padding of container.</param>
        /// <returns>View margin container was created on.</returns>
        public static UIView HorisontalSpace(this UIView parentView,
            int height = 20,
            Action<UIView> containerProcessingAction = null)
        {
            // Create the container.
            var frame = new CGRect(0,
                0, 
                parentView.Bounds.Width, 
                height);
            var lastChild = parentView
                .Subviews
                .Reverse()
                .FirstOrDefault();

            // If another view exists prior to this one, set this view relative to that one.
            if (lastChild != null)
            {
                frame = new CGRect(
                    0,
                    lastChild.Frame.Bottom,
                    parentView.Bounds.Width,
                    height);
            }
            
            var container = new UIView
            {
                Frame = frame
            };

            // Execute the processing that has to occur on the container.
            containerProcessingAction?.Invoke(container);
            // Add the created container to the parent.
            parentView.AddSubview(container.DecorateForDebugging());
            
            // Return parent for fluent programming.
            return parentView;
        }
    }
}