﻿using System.Collections.Generic;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.System
{
    /// <summary>
    /// Debugger extensions for UI controls.
    /// </summary>
    public static class DebuggerExtensions
    {
        /// <summary>
        /// Color index to use in debugger.
        /// </summary>
        private static int _currentSelectedColorIndex = 0;

        /// <summary>
        /// Whether or not the application is in debug mode.
        /// </summary>
        #if DEBUG
            private static bool _debugMode = false;
        #else
            private static bool _debugMode = false;
        #endif
        
        /// <summary>
        /// Decorate a view to display debugging information.
        /// NOTE: This method should be called lastly after appending child controls in order to overlay them.
        /// </summary>
        /// <param name="parentView">View to decorate.</param>
        /// <returns>Decorated view.</returns>
        public static UIView DecorateForDebugging(this UIView parentView)
        {
            if (!_debugMode) return parentView;
            
            // Grab a random color we will use for debugging guides.
            UIColor debuggerColor = GetRandomColor();
            
            // Add some borders to the view.
            parentView.Layer.BorderColor = debuggerColor.CGColor;
            parentView.Layer.BorderWidth = 2;
            parentView.Layer.MasksToBounds = true;
            
            return parentView;
        }

        /// <summary>
        /// Generate a random ui color.
        /// </summary>
        /// <returns>Generated ui color.</returns>
        private static UIColor GetRandomColor()
        {
            var colors = new List<UIColor>
            {
                UIColor.Black,
                UIColor.Blue,
                UIColor.Brown,
                UIColor.Cyan,
                UIColor.DarkGray,
                UIColor.Gray,
                UIColor.Green,
                UIColor.LightGray,
                UIColor.Magenta,
                UIColor.Orange,
                UIColor.Purple,
                UIColor.Red,
                UIColor.White,
                UIColor.Yellow,
            };

            UIColor result = colors[_currentSelectedColorIndex++];

            // Reset color index.
            if (_currentSelectedColorIndex >= colors.Count)
            {
                _currentSelectedColorIndex = 0;
            }
            
            return result;
        }
    }
}