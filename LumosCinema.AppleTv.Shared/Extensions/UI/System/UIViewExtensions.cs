﻿using UIKit;

namespace LumosCinema.AppleTv.Shared.Extensions.UI.System
{
    /// <summary>
    /// General UIView extensions.
    /// </summary>
    public static class UIViewExtensions
    {
        /// <summary>
        /// Fluent way to clear all children inside of a view.
        /// </summary>
        /// <param name="parentView">View to remove children from.</param>
        /// <returns>View without children.</returns>
        public static UIView Clear(this UIView parentView)
        {
            foreach (UIView childView in parentView.Subviews)
            {
                childView.RemoveFromSuperview();
            }
            
            return parentView;
        }
    }
}