﻿using SkiaSharp;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Models.Configuration
{
    /// <summary>
    /// Hex color container.
    /// </summary>
    public class HexColorModel
    {
        /// <summary>
        /// String hex color value.
        /// </summary>
        public string HexString { get; set; }

        /// <summary>
        /// Convert hex string to SKColor.
        /// </summary>
        /// <returns></returns>
        public SKColor ToSkColor()
        {
            return SKColor.Parse(HexString);
        }
        
        /// <summary>
        /// Convert hex string to UI color.
        /// </summary>
        /// <returns>UIColor.</returns>
        public UIColor ToUiColor()
        {
            SKColor skColor = ToSkColor();
            
            return UIColor.FromRGB(skColor.Red, skColor.Green, skColor.Blue);
        }
        
        /// <summary>
        /// Convert hex string to UI color.
        /// </summary>
        /// <param name="opacity">Opacity of the color. 0 - 255</param>
        /// <returns>UIColor.</returns>
        public UIColor ToUiColor(int opacity)
        {
            SKColor skColor = ToSkColor();
            
            return UIColor.FromRGBA(skColor.Red, skColor.Green, skColor.Blue, opacity);
        }
    }
}