﻿using System.ComponentModel;
using System.Threading.Tasks;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using FrostAura.Libraries.Core.Models.Reactive;

namespace LumosCinema.AppleTv.Shared.Models.Configuration
{
    /// <summary>
    /// Configuration required for a Plex integration.
    /// </summary>
    public class PlexConfigurationModel : BaseConfigurationModel
    {   
        /// <summary>
        /// Plex username / email address.
        /// </summary>
        [Description("Plex.tv Email")]
        public IObservedValue<string> Email { get; } = new Observed<string>();
        
        /// <summary>
        /// Plex password.
        /// </summary>
        [Description("Plex.tv Password")]
        public IObservedValue<string> Password { get; } = new Observed<string>();
        
        public PlexConfigurationModel()
        {
            Identifier.Value = "Plex Media Server";
            SaveButtonIdentifier.Value = "Test Connection";
        }

        protected override async Task SaveAsync()
        {
            // TODO: Plex connection test.
            // TODO: Upon any input change, IsValid should be false.
        }
    }
}