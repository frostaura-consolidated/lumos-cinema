﻿using System.Threading.Tasks;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using FrostAura.Libraries.Core.Models.Reactive;
using UIKit;

namespace LumosCinema.AppleTv.Shared.Models.Configuration
{
    /// <summary>
    /// Base integration configuration model.
    /// </summary>
    public abstract class BaseConfigurationModel
    {
        /// <summary>
        /// Section identifier.
        /// </summary>
        public IObservedValue<string> Identifier { get; } = new Observed<string>();
        
        /// <summary>
        /// Section save button identifier.
        /// </summary>
        public IObservedValue<string> SaveButtonIdentifier { get; } = new Observed<string>();
        
        /// <summary>
        /// Whether or not the section is in a loading state.
        /// </summary>
        public IObservedValue<bool> IsLoading { get; } = new Observed<bool>();
        
        /// <summary>
        /// Whether or not the configuration is correct.
        /// </summary>
        public IObservedValue<bool> IsValid { get; } = new Observed<bool>();

        /// <summary>
        /// Save for internal usage.
        /// </summary>
        public async Task SaveInternalAsync()
        {
            IsLoading.Value = true;

            await SaveAsync();
            
            IsLoading.Value = false;
        }

        /// <summary>
        /// Convert the model to a UIView.
        /// </summary>
        /// <param name="parentView">Which view to add the controls to.</param>
        /// <returns></returns>
        public UIView ToUiView(UIView parentView)
        {
            return parentView;
        }
        
        /// <summary>
        /// Abstract method to save configuration. Perform connections required. Tests etc.
        /// </summary>
        protected abstract Task SaveAsync();
    }
}