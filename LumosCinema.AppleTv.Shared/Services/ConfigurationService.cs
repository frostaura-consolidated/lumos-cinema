﻿using FrostAura.Libraries.Core.Abstractions;
using LumosCinema.AppleTv.Shared.Models.Configuration;

namespace LumosCinema.AppleTv.Shared.Services
{
    /// <summary>
    /// Singleton configuration service.
    /// </summary>
    public class ConfigurationService : Singleton<ConfigurationService>
    {
        #region Lights
        
        // TODO: Phillips Hue
        
        #endregion
        
        #region Media Server

        public PlexConfigurationModel PlexConfiguration { get; } = new PlexConfigurationModel();
        
        #endregion
    }
}