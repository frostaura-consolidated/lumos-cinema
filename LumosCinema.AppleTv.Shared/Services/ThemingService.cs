﻿using FrostAura.Libraries.Core.Abstractions;
using FrostAura.Libraries.Core.Extensions.Reactive;
using FrostAura.Libraries.Core.Interfaces.Reactive;
using LumosCinema.AppleTv.Shared.Models.Configuration;

namespace LumosCinema.AppleTv.Shared.Services
{
    /// <summary>
    /// Service for providing theming functionality.
    /// </summary>
    public class ThemingService : Singleton<ThemingService>
    {
        /// <summary>
        /// Primary background color.
        /// </summary>
        public IObservedValue<HexColorModel> BackgroundColor { get; } = new HexColorModel { HexString = "#003264"}.AsObservedValue();
        
        /// <summary>
        /// Primary color used for titles and things of importance.
        /// </summary>
        public IObservedValue<HexColorModel> PrimaryColor { get; } = new HexColorModel { HexString = "#0096ff"}.AsObservedValue();
        
        /// <summary>
        /// Secondary color used for paragraths and things of less importance.
        /// </summary>
        public IObservedValue<HexColorModel> SecondaryColor { get; } = new HexColorModel { HexString = "#ffffff"}.AsObservedValue();
        
        /// <summary>
        /// Unimportant color used for disabled things and things of the least importance.
        /// </summary>
        public IObservedValue<HexColorModel> UnimportantColor { get; } = new HexColorModel { HexString = "#969696"}.AsObservedValue();
    }
}